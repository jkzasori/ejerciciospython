import requests
import json


if __name__ == '__main__':
    url = 'https://jsonplaceholder.typicode.com/users'
    url2 = 'https://jsonplaceholder.typicode.com/todos'
    url3 = 'https://jsonplaceholder.typicode.com/comments'
    url4 = 'https://jsonplaceholder.typicode.com/photos'
    response = requests.get(url)
    response2 = requests.get(url2)
    response3 = requests.get(url3)
    response4 = requests.get(url4)
    if response.status_code == 200 and response2.status_code == 200 and response3.status_code == 200 and response4.status_code == 200:
        file=open('indexTamara.html','w')
        photos='<div class="row white"><h2>FOTOS</h2>'
        fo=0
        for i in response4.json():
            photos=photos+'<div class="col s10 m3 l4"><div class="card hoverable" style="cursor: pointer"><div class="card-image"><img src="'+str(response4.json()[fo].get('url'))+'"><span class="card-title">'+str(response4.json()[fo].get('title'))+'</span></div><div class="card-content"><p>Img # '+str(response4.json()[fo].get('id'))+'</p></div><div class="card-action"><a href="'+str(response4.json()[fo].get('url'))+' target="_blank">This is a link of photo</a></div></div></div>'
            fo=fo+1
            if fo==12:
                break;
        photos=photos+'</div>'
        users='<div class="white"><h2>Users</h2><table class="responsive-table"><thead><tr><th class="animated infinite bounce">ID</th><th>Name</th><th>Username</th><th>Email</th><th>Phone</th><th>Website</th></tr></thead><tbody>'
        j=0
        for i in response.json():
            users=users+'<tr class="hoverable" style="cursor: pointer"><td class="animated bounce">'+str(response.json()[j].get('id'))+'</td><td>'+str(response.json()[j].get('name'))+'</td><td>'+str(response.json()[j].get('username'))+'</td><td>'+str(response.json()[j].get('email'))+'</td><td>'+str(response.json()[j].get('phone'))+'</td><td>'+str(response.json()[j].get('website'))+'</td></tr>'
            j=j+1
            if j==10:
                break;
        users=users+'</tbody></table></div>'
        todos='<div class="white"><h2>TO DO</h2><table class="responsive-table"><thead><tr><th class="animated infinite bounce">UserID</th><th>ID</th><th>Title</th><th>Completed</th></tr></thead><tbody>'
        tt=0
        for x in response2.json():
            todos=todos+'<tr class="hoverable" style="cursor: pointer"><td>'+str(response2.json()[tt].get('userId'))+'</td><td>'+str(response2.json()[tt].get('id'))+'</td><td>'+str(response2.json()[tt].get('title'))+'</td><td>'+str(response2.json()[tt].get('completed'))+'</td></tr>'
            tt=tt+1
            if tt==10:
                break;
        todos=todos+'</tbody></table></div>'
        comments='<div class="center-align white"><h2>Comentarios</h2>'
        t3=0
        for x in response3.json():
            comments = comments+'<div class="hoverable" style="cursor: pointer;background-color: #dad9d9;border: 1px solid gray;margin-bottom: 10px;"><h4>'+str(response3.json()[t3].get('name'))+'<br>'+str(response3.json()[t3].get('email'))+'</h4>'+'<p>'+str(response3.json()[t3].get('body'))+'</p></div>'
            t3=t3+1
            if t3==10:
                break;
        comments=comments+'</div>'
        print(comments)
        file.write(str('<!DOCTYPE html>'
            '<html>'
                '<head>'
                    '<title>Consumiendo API''s</title>'
                    '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">'
                    '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">'
                    '<!-- Compiled and minified JavaScript -->'
                    '<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>'
                    '<meta name="viewport" content="width=device-width, initial-scale=1">'
                    '<style>'
                       'body{text-align:center} '
                       'h2{font-weight:bold;font-size:2.56rem; padding:10px;border-bottom: 1px solid #e4e0e0;text-transform: uppercase;}'
                       '.card .card-image .card-title{font-size: 1.2rem;}'
                       '</style>'
                '</head>'
                '<body style="background-color:#e4e0e0"><div class="container">'
                +photos+users+todos+comments+
                '</div></body>'
            '</html>'))
        print(str(response.json()[0].get('id')))
        file.close()
    else:
        print("Nada")
"""import urllib.request
import json
if __name__ == '__main__':
    url = 'https://jsonplaceholder.typicode.com/users'
    a=open('index.html','w')
    response = json.load(urllib.request.urlopen(url))
    longi=len(str(response))
    a.write(str(longi))
    print(response)"""