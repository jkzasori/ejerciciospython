NombreEquipo=[]
PartidosJugados=[]
PartidosGanados=[]
PartidosEmpatados=[]
PartidosPerdidos=[]
GolesAFavor=[]
GolesEnContra=[]
PuntosEquipo=[]
numeroEquipo=0
jugado=0
ganados=0
perdidos=0
empatados=0
aFavor=0
puntos=0
enContra=0

while True:
    try:
        numeroEquipo=int(input("Digite la cantidad de equipos en torneo: "))
        break
    except:
        print("Ingrese un número de equipos válido")
if (numeroEquipo!=0):
    for i in range(1, numeroEquipo+1):
        nombre=input("Ingrese el nombre del equipo "+str(i)+": ")

        while True:
            try:
                jugados = int(input("Ingrese los partidos jugados por el equipo "+str(nombre)+": "))
                break
            except:
                print("Por favor ingrese un número")
        while True:
            try:
                ganados = int(input("Ingrese los partidos ganados por el equipo "+str(nombre)+": "))
                break
            except:
                print("Por favor ingrese un número")
        while True:
            try:
                perdidos = int(input("Ingrese los partidos perdidos por el equipo " + str(nombre) + ": "))
                break
            except:
                print("Por favor ingrese un número")
        if((ganados+perdidos)>jugados):
            print("NOTA: La suma de los partidos ganados y perdidos no puede ser superior a la cantidad de partidos jugados, vuelva ingresar los datos. ")
            while True:
                try:
                    ganados = int(input("Ingrese los partidos ganados por el equipo " + str(nombre) + ": "))
                    break
                except:
                    print("Por favor ingrese un número")
            while True:
                try:
                    perdidos = int(input("Ingrese los partidos perdidos por el equipo " + str(nombre) + ": "))
                    break
                except:
                    print("Por favor ingrese un número")
        if((ganados+perdidos)<jugados):
            empatados=jugados-(ganados+perdidos)
        if((ganados+perdidos)==jugados):
            empatados=0
        while True:
            try:
                aFavor = int(input("Ingrese los goles a favor para el equipo " + str(nombre) + ": "))
                break
            except:
                print("Por favor ingrese un número")
        while True:
            try:
                enContra = int(input("Ingrese los goles a enContra para el equipo " + str(nombre) + ": "))
                break
            except:
                print("Por favor ingrese un número")
        puntos=(ganados*3)+(empatados*1)

        PuntosEquipo.append(puntos)
        NombreEquipo.append(nombre)
        PartidosJugados.append(jugados)
        PartidosEmpatados.append(empatados)
        PartidosPerdidos.append(perdidos)
        PartidosGanados.append(ganados)
        GolesAFavor.append(aFavor)
        GolesEnContra.append(enContra)

auxT=0
auxT2=0
auxT3=0
auxT4=0
auxT5=0
auxT6=0
auxT7=0
auxT8=0
for i in range(0,numeroEquipo):
    for j in range(0, numeroEquipo):
        if(PuntosEquipo[i]>=PuntosEquipo[j]):
            auxT = PuntosEquipo[i]
            auxT2 = NombreEquipo[i]
            auxT3 = PartidosJugados[i]
            auxT4 = PartidosEmpatados[i]
            auxT5 = PartidosPerdidos[i]
            auxT6 = PartidosGanados[i]
            auxT7 = GolesAFavor[i]
            auxT8 = GolesEnContra[i]
            PuntosEquipo[i] = PuntosEquipo[j]
            NombreEquipo[i] = NombreEquipo[j]
            PartidosJugados[i] = PartidosJugados[j]
            PartidosEmpatados[i] = PartidosEmpatados[j]
            PartidosPerdidos[i] = PartidosPerdidos[j]
            PartidosGanados[i] = PartidosGanados[j]
            GolesAFavor[i] = GolesAFavor[j]
            GolesEnContra[i] = GolesEnContra[j]
            PuntosEquipo[j] = auxT
            NombreEquipo[j] = auxT2
            PartidosJugados[j] = auxT3
            PartidosEmpatados[j] = auxT4
            PartidosPerdidos[j] = auxT5
            PartidosGanados[j] = auxT6
            GolesAFavor[j] = auxT7
            GolesEnContra[j] = auxT8
Marcador=[]
print("Equipo ||||| P.J | P.G | P.E | P.P | G.F | G.C | G.D | Puntos")
for i in range(0, numeroEquipo):
    Marcador.append(str(NombreEquipo[i])+"   |   "+str(PartidosJugados[i])+"  ||  "+str(PartidosGanados[i])+"  ||  "+str(PartidosEmpatados[i])+"  ||  "+str(PartidosPerdidos[i])+"  ||  "+str(GolesAFavor[i])+"  ||  "+str(GolesEnContra[i])+"  ||  "+str(GolesAFavor[i]-GolesEnContra[i])+"  ||  "+str(PuntosEquipo[i]))

for i in range(0, numeroEquipo):
    print(""+str(Marcador[i]))