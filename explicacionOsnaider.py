#function's with * and **
def test_argument(*args):
    print(args)
def test_kwargs(**kwargs):
    print(kwargs)
variable="jojojo"
test_argument(variable)#devuelve tupla
test_kwargs(name=variable)#devuelve diccionario

def suma(*args):
    return sum(args)
print(suma(1,2,3,4,5,6,7,8,9))

def saludo(**kwargs):
    pais= kwargs.get('nacionalidad')
    if pais=='Colombia':
        return 'Hola mi nombre es {nombre} y mi edad es {edad} años y soy {nacionalidad}'.format(**kwargs)
    else:
        return 'Hola mi nombre es {nombre} y mi edad es {edad} años'.format(**kwargs)

#yo={'nombre':'Támara', 'edad':'199'}
print(saludo(nombre='Támara',edad='199', pais='Colombia'))

