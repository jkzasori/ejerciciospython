"""import http.server
import socketserver
PORT = 3002
Handler = http.server.SimpleHTTPRequestHandler

with socketserver.TCPServer(('', PORT), Handler) as httpd:
	print("Servidor en el puerto: ", PORT)
	print()
	httpd.serve_forever()
"""

from http.server import HTTPServer, BaseHTTPRequestHandler
import os
rutas ={'/':'index.html','/about':'about.html','/contact':'contact.html','/style.css':'style.css','/indexTamara':'indexTamara.html'}
class ClasesitaServer(BaseHTTPRequestHandler):
	def do_GET(self):
		if self.path =='/style.css':
			content = self.handle_http(200, 'text/css')
			self.wfile.write(content)
		if self.path =='/' or self.path =='/about' or self.path =='/contact' or self.path=='/indexTamara':
			content = self.handle_http(200, 'text/html')
			self.wfile.write(content)
		else:
			content = self.handle_http(404, 'text/html')
			print('nada')
			self.wfile.write(content)
	def handle_http(self, status, conten_type):
		self.send_response(status)
		self.end_headers()
		if status == 200:
			routes_content = rutas[self.path]
			f = open(routes_content, 'r')
		elif status == 404:
			f = open('index.html', 'r')
		return bytes(f.read(), 'UTF-8')
def run(server_class=HTTPServer, handler_class=ClasesitaServer, port=3003):
	server_address = ('', port)
	httpd = server_class(server_address, handler_class)
	print('Corriendo en el puerto', port)
	httpd.serve_forever()

run()
