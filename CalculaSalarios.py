
#Twork=0 #este a en días
#SM=0  Número de Salario Mínimo
#HED=0 #Horas extras diurnas trabajadas
#HN=0 #Horas nocturnas trabajadas
#HEN=0#Horas extras nocturnas
#HED_F=0#Horas extras diurnas dominicales y festivos
#HEND_F=0#Horas extras Nocturnas dominicales festivos
AUX_T=72250#Auxilio de transporte
SalarioMinimo=850000
continueRegist=''
list=[]
def SMensual(sm, num): #Calcula el valor del salario mensual
    return sm*num
while True:
    name=input("Ingrese el nombre de la Persona: ")
    lastName=input("Ingrese el apellido: ")
    while True:
        try:
            SM=float(input("Ingrese cantidad de salarios mínimos ganados por el señor(a) "+name+" "+lastName+": "))
            if SM>0:
                break
            else:
                print("Recuerde que el # debe ser positivo mayor que cero")
        except:
            print("Recuerde que el salario mímino debe ser un número")
    while True:
        try:
            Twork=float(input("Ingrese el tiempo laborado en días por el señor(a) "+name+" "+lastName+": "))
            if Twork>0:
                break
            else:
                print("Recuerde que el # tiempo laborado debe ser positivo mayor que cero")
        except:
            print("Recuerde que el tiempo laborado debe ser positivo mayor que cero")
    while True:
        try:
            HED=float(input("Ingrese el # de horas extras diurnas trabajadas por el señor(a) "+name+" "+lastName+": "))
            if HED>=0:
                break
            else:
                print("Recuerde que el # horas extras diurnas trabajadas: ")
        except:
            print("Recuerde que el # horas extras diurnas trabajadas debe ser un número positivo mayor que cero")
    while True:
        try:
            HN=float(input("Ingrese el # de horas Nocturnas trabajadas por el señor(a) "+name+" "+lastName+": "))
            if HN>=0:
                break
            else:
                print("Recuerde que el # horas Nocturnas trabajadas debe ser un número: ")
        except:
            print("Recuerde que el # horas Nocturnas trabajadas debe ser un número positivo")
    while True:
        try:
            HEN=float(input("Ingrese el # de horas extras nocturnas trabajadas por el señor(a) "+name+" "+lastName+": "))
            if HEN>=0:
                break
            else:
                print("Recuerde que el # horas extras nocturnas trabajadas debe ser un número positivo: ")
        except:
            print("Recuerde que el # horas extras nocturnas trabajadas debe ser un número positivo mayor que cero")
    while True:
        try:
            HED_F=float(input("Ingrese el # de horas extras ordinarias dominicales y festivos trabajadas por el señor(a) "+name+" "+lastName+": "))
            if HED_F>=0:
                break
            else:
                print("Recuerde que el # horas extras dominicales y festivos trabajadas debe ser positivo mayor que cero: ")
        except:
            print("Recuerde que el # horas extras dominicales y festivos trabajadas debe ser un número positivo mayor que cero")
    while True:
        try:
            HEND_F=float(input("Ingrese el # de horas extras Nocturnas dominicales y festivos trabajadas por el señor(a) "+name+" "+lastName+": "))
            if HEND_F>=0:
                break
            else:
                print("Recuerde que el # horas extras Nocturnas dominicales y festivos trabajadas debe ser positivo mayor que cero: ")
        except:
            print("Recuerde que el # horas extras Nocturnas dominicales y festivos trabajadas debe ser un número positivo mayor que cero")
    sMonth = round(SMensual(SalarioMinimo,SM),2)  #valor de salario mensual
    vhOrdinaria = round(sMonth/240,2)     #valor de una hora ordinaria
    vhED = round(vhOrdinaria*1.25*HED,2) #Total del valor de horas extras diurnas
    vhN = round(vhOrdinaria*1.35*HN,2) #Valor total de hora nocturna
    vhEN = round(vhOrdinaria*1.75*HEN,2) #Valor total de hora extra nocturna
    vhEDF = round(vhOrdinaria*2*HED_F,2) #Valor total de hora extra dominical y festivo
    vhENDF = round(vhOrdinaria*2.5*HEND_F,2) #Valor total de hora extra nocturna dominical y festivo
    cesantia = round((sMonth*Twork)/360,2) #Calculo de cesantías
    iporCesantia = round((cesantia*Twork*0.12)/360,2) #Calculo de Intereses por cesantías
    primaServicio = round((sMonth*(Twork/(30*6)))/360,2) #Cálculo de prima
    vacaciones = round((sMonth*Twork)/720,2) #calculo de vacaciones
    netopagado = round(vacaciones+primaServicio+iporCesantia+cesantia+vhENDF+vhEDF+vhEN+vhN+vhED+sMonth,2)
    dicsRegist = {}
    dicsRegist.update({'name': name, 'lastName': lastName,
                       'SMONTG':sMonth, 'VhOrdinaria':vhOrdinaria,
                       'vHED':vhED, 'vhN':vhN,'vhEN':vhEN,
                       'vhEDF':vhEDF, 'vhENDF':vhEDF, 'cesantia':cesantia,
                       'iporCesantia':iporCesantia,'primaServicio':primaServicio,
                       'vacaciones':vacaciones,'neto':netopagado})
    list.append(dicsRegist)
    continueRegist=input("\n¿Desea continuar ingresando registros?(responda si o no): ").lower()
    if continueRegist=="no":
        break
print("\n\n\n\n\n*********************************RESULTADO*********************************")
print(list)
netoTotal=0
for i in range(len(list)):
     netoTotal=float(netoTotal+list[i].get('neto') )
print("\n***El neto que paga la empresa es: ",netoTotal,"***")