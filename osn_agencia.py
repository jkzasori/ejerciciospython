import urllib.request
import urllib.parse

class AgenciaTurismo:
    Agencia_lista = []

    def __init__(self, codigo, nombre, direccion):
        self.codigo = codigo
        self.nombre = nombre
        self.direccion = direccion
        self.conductores = []

    def listarAgencia(self, agenciaLista):
        print("\n***Mis Agencias***")
        for i in agenciaLista:
            print(f'Nombre: {i.nombre}\nCodigo: {i.codigo}\nDireccion: {i.direccion}')
            for t in i.conductores:
                print(f'  Conductores: {t.nombre} {t.apellido}')
            print("\n")
        print("****************************")

    def agregaConductor(self, elConductor):
        elConductor.codigo_agencia = self.codigo
        self.conductores.append(elConductor)


agencia = AgenciaTurismo(codigo=1, nombre='Agencia Travel', direccion='Calle travel Cra. Travel')
agencia.Agencia_lista.append(agencia)
agencia2 = AgenciaTurismo(codigo=2, nombre='Agencia SaSaSa', direccion='Calle SaSaSa Cra. Sasasa')
agencia.Agencia_lista.append(agencia2)
agencia3 = AgenciaTurismo(codigo=3, nombre='Agencia Támara', direccion='Calle Támara Cra. Támara')
agencia.Agencia_lista.append(agencia3)


class Conductores:
    listaconductores = []

    def __init__(self, identificacion, codigo, nombre, apellido, sexo, edad, nacionalidad):
        self.identificacion = identificacion
        self.codigo = codigo
        self.nombre = nombre
        self.apellido = apellido
        self.sexo = sexo
        self.edad = edad
        self.nacionalidad = nacionalidad
        self.codigo_agencia = ''

    def listarConductores(self, listaConductor):
        print("\n***Lista del total de conductores***")
        for i in listaConductor:
            print(f'\nIdentificación: {i.identificacion}\nCódigo: {i.codigo}'
                  f'\nNombre: {i.nombre}\nApellido: {i.apellido}\nSexo: {i.sexo}'
                  f'\nEdad: {i.edad}\nNacionalidad: {i.nacionalidad}\nCodigo_agencia: {i.codigo_agencia}')
        print("**************************\n")


conductor = Conductores(identificacion=12345, codigo=11, nombre='Pepa', apellido='Pig', sexo='F', edad=35,
                        nacionalidad='Pepalandia')
conductor.listaconductores.append(conductor)
conductor2 = Conductores(identificacion=56742, codigo=22, nombre='Ratón', apellido='Perez', sexo='M', edad=55,
                         nacionalidad='Ratónlandia')
conductor2.listaconductores.append(conductor2)
conductor3 = Conductores(identificacion=23456, codigo=33, nombre='Papapepa', apellido='Pig', sexo='M', edad=75,
                         nacionalidad='Pepalandia')
conductor3.listaconductores.append(conductor3)
conductor4 = Conductores(identificacion=34567, codigo=44, nombre='Luna', apellido='Noche', sexo='F', edad=5500,
                         nacionalidad='Universo')
conductor4.listaconductores.append(conductor4)
conductor5 = Conductores(identificacion=45678, codigo=55, nombre='Sol', apellido='Día', sexo='M', edad=35000000,
                         nacionalidad='Universo')
conductor5.listaconductores.append(conductor5)
conductor6 = Conductores(identificacion=56789, codigo=66, nombre='Van', apellido='Helsing', sexo='M', edad=35,
                         nacionalidad='Oscuridad')
conductor6.listaconductores.append(conductor6)

agencia.agregaConductor(conductor)  # Agrega a agencia
agencia.agregaConductor(conductor2)  # Agrega a agencia
agencia2.agregaConductor(conductor3)  # Agrega a agencia
agencia2.agregaConductor(conductor4)  # Agrega a agencia
agencia3.agregaConductor(conductor5)  # Agrega a agencia
agencia3.agregaConductor(conductor6)  # Agrega a agencia
#print(agencia.listarAgencia(agencia.Agencia_lista))  # Muestra agencia a agencia
#conductor.listarConductores(conductor.listaconductores)  # Muestra Conductor


class Turista:
    listaTurista = []

    def __init__(self, identificacion, nombre, apellido, nacionalidad, sexo, edad):
        self.identificacion = identificacion
        self.nombre = nombre
        self.apellido = apellido
        self.nacionalidad = nacionalidad
        self.sexo = sexo
        self.edad = edad

    def listarTurista(self, listaturista):
        print("\n***Turistas***")
        for i in listaturista:
            print(f'Identificación: {i.identificacion}\nNombre: {i.nombre}'
                  f'Apellido: {i.apellido}\nNacionalidad: {i.nacionalidad}'
                  f'\nSexo: {i.sexo}\nEdad: {i.edad}\n')
        print("***********************")


turista = Turista(identificacion=10909, nombre='Ada', apellido='Madrina', nacionalidad='Adalandia', sexo='F', edad=1000)
turista.listaTurista.append(turista)
turista2 = Turista(identificacion=10808, nombre='Perro', apellido='de la Calle', nacionalidad='Calle', sexo='M', edad=1)
turista.listaTurista.append(turista2)
turista3 = Turista(identificacion=10707, nombre='Gata', apellido='de la Calle', nacionalidad='Calle', sexo='F',
                   edad=200)
turista3.listaTurista.append(turista3)
turista4 = Turista(identificacion=10606, nombre='Mago', apellido='de Os', nacionalidad='Adalandia', sexo='M', edad=4230)
turista4.listaTurista.append(turista4)
turista5 = Turista(identificacion=10404, nombre='Canario', apellido='Vuelo', nacionalidad='Bosque', sexo='M', edad=55)
turista5.listaTurista.append(turista5)
turista6 = Turista(identificacion=10303, nombre='Gallina', apellido='vieja', nacionalidad='Bosque', sexo='F', edad=25)
turista6.listaTurista.append(turista6)
#turista.listarTurista(turista.listaTurista) #Lista de turistas


class Tours:
    listatour = []

    def __init__(self, codigo, destino, fecha, costo, duracionD):
        self.codigo = codigo
        self.destino = destino
        self.fecha = fecha
        self.costo = costo
        self.duracionD = duracionD
        self.codigo_conductor = ''
        self.turista = []

    def listar_tour(self, listatour):
        print("\n****Total de Tours****")
        for i in listatour:
            print(f'Código: {i.codigo}\nDestino: {i.destino}'
                  f'Fecha: {i.fecha}\nCosto: {i.costo}\n'
                  f'Duración: {i.duracionD}\nCodigo Conductor: {i.codigo_conductor}')
            for tu in i.turista:
                print(f'  Turista: {tu.nombre}\n')
        print("***************")

    def agregarConductor(self, elconductor):
        self.codigo_conductor = elconductor.codigo

    def agregarTurista(self, elturista):
        self.turista.append(elturista)


tour = Tours(codigo="t1", destino='Blue Sky', fecha='12/12/12', costo=20000, duracionD=2)
tour.listatour.append(tour)
tour2 = Tours(codigo="t2", destino='Red Sky', fecha='15/10/15', costo=555550, duracionD=3)
tour2.listatour.append(tour2)
tour3 = Tours(codigo="t3", destino='Black Sky', fecha='18/08/18', costo=66666620, duracionD=6)
tour3.listatour.append(tour3)
tour4 = Tours(codigo="t4", destino='Gray Sky', fecha='11/06/11', costo=556620, duracionD=4)
tour4.listatour.append(tour4)
tour5 = Tours(codigo="t5", destino='Green Sky', fecha='22/04/22', costo=23411620, duracionD=23)
tour5.listatour.append(tour5)
tour6 = Tours(codigo="t6", destino='Violet Sky', fecha='3/1/18', costo=66666620, duracionD=7)
tour6.listatour.append(tour6)

tour.agregarConductor(conductor)
tour2.agregarConductor(conductor2)
tour3.agregarConductor(conductor3)
tour4.agregarConductor(conductor4)
tour5.agregarConductor(conductor5)
tour6.agregarConductor(conductor6)

tour.agregarTurista(turista)
tour2.agregarTurista(turista2)
tour3.agregarTurista(turista3)
tour4.agregarTurista(turista4)
tour5.agregarTurista(turista5)
tour6.agregarTurista(turista6)

#tour.listar_tour(tour.listatour) #lista el todos los tures


# sorted(lista, key=lambda x: x['edad])
class Operacion:
    def maximo(self, lista, filtrador):
        self.filtrador = filtrador
        # lista = lista
        maximo = max(lista, key=lambda x: x.__getattribute__(filtrador))
        return maximo

    def contador(self, lista):
        return len(lista)

    def mostrador(self, objeto, campo):
        campo = campo
        i = objeto
        res = i.__getattribute__(campo)
        return res
    def agrupador(self, objeto, campo, valor, valorMostrar):
        resp = filter(lambda x: x.__getattribute__(campo) == valor, objeto)
        # print(objeto.__dict__)
        count=0
        for i in resp:
            count=count+1
            print("  ",i.__getattribute__(valorMostrar))
        return count


operaciones = Operacion()

print("\n\n")
print("Total de Conductores: ", operaciones.contador(conductor.listaconductores))
print("Total de Turistas: ", operaciones.contador(turista.listaTurista))
print("El tour con mayor duración fue: ",
      operaciones.mostrador(operaciones.maximo(tour.listatour, 'duracionD'), 'destino'))

print("*Turistas Femeninos")
print('   El número de turistas femeninos es: ', operaciones.agrupador(turista.listaTurista, 'sexo', 'F', 'nombre'),'\n________________')

print("Turistas Masculinos")
print('   El número de turistas másculinos es: ', operaciones.agrupador(turista.listaTurista, 'sexo', 'M', 'nombre'),'\n________________')

print("Turistas de Nacionalidad de Adalandia")
print('   El número de Nacionalidad de Adalandia es: ', operaciones.agrupador(turista.listaTurista, 'nacionalidad', 'Adalandia', 'nombre'),'\n________________')

print("Turistas de Nacionalidad Calle")
print('   El número de Nacionalidad Calle es: ', operaciones.agrupador(turista.listaTurista, 'nacionalidad', 'Calle', 'nombre'),'\n________________')

print("Turistas de Nacionalidad Bosque")
print('   El número de Nacionalidad Bosque es: ', operaciones.agrupador(turista.listaTurista, 'nacionalidad', 'Bosque', 'nombre'),'\n________________')



#print("Agencia: ", operaciones.agrupador(tour.listatour, 'conductor', tour.listatour, 'nombre'))