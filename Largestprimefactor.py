"""
The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?
"""

#Imprimir los #s primos, en un rango dado.
Mayor=0
def primosRango(a,b, count):
    for i in range(a,b+1):
        if(i%2!=0 and i!=1):
           # print("El número "+str(i)+" es primo")
            for j in range(1, i+1):
                if(i%j==0):
                    count = count +1
           #print ("contador "+str(count))
            if(count==2):
                if 600851475143%i==0:
                    #print("Es divi: ", i)
                    Mayor=i
                #print("El número " + str(i) + " es primo")
            count =0
    print("\n***El número es: ", Mayor, "***")
a=2
b=13195
count = 0
if(a>=b):
    primosRango(b, a, count)
else:
    primosRango(a, b, count)