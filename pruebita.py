
def algo():
    a ="Esto es algo"
def otroAlgo():
    b = "Esto es otra cosa"
    print(b)

otroAlgo()
#lISTAS**********
#*EXTEND() - agrega los elementos a la lista en la primera posición o al final. sirve para unir dos listas
listl = [1,2,3,4,5,6,7,8,9,0]
print("Original: ",listl)
listl.extend([11,2,22,33,44,55,2])
print("  Extend: ",listl)
#*REMOVE() - elimina un elemento que se le pase como argumento
listl.remove(2)
print("  Remove: ",listl)
#*POP() - Quita el inten de la posición dada de la lista y lo devuelve, si no le pasas nada te elimina el último
listl.pop(0)
print("     POP: ", listl)
print("Elemento eliminado con pop: ",listl.pop())
#*CLEAR  - Limpia todos los elementos de la lista
print(listl.clear())
print(listl)
#*COUNT - devuelve el número de elementos que aparece el elemento en la
# lista, se le pasa como parametros el elemento que aparece en la lista
listl=[1,2,3,4,51,2,2,1,2]
print("   COUNT: ",listl.count(2))
#*INSERT - recibe dos parámetros el valor de la pocición y lo que vas a meter

listl.insert(1,23)
print("  INSERT: ",listl)
#*SORT - ordena la lista de forma acscendente
listFruit=["manzana","peras", "manzanas","uvas","peras","uvas"]
listFruit.sort(reverse=True) #Con reverse=True lo organiza de forma descendente
print(listFruit)
varString="Esta es una variable cadena"
print(varString.split())

#Dicionarios****************
persona = {'name': "José",
           'lastName':"Támara",
           'addres':'Turbaco Calle San Plablo',
           'city':'Turbaco',
           'country':'Colombia',
           'age':99,
           'celphone':1234567890,
           'profesion':'Engineer'}
for key in persona:
    print(persona[key])
#tupla pasa a diccionario
varDic = dict(Name='Jonathan', LastName='Martinez')
print(varDic)
#otra forma de crear diccionario a partir de una lista
varDic2=dict(zip('abcd',[1,2,3,4]))
print(varDic2)

#item devuleve una lista de tublas
j=varDic2.items()
print(j)
#keys
otroDiccionario={'r':'es ere','b':'es b'}
d=otroDiccionario.keys()
print(d)
#values
dicValue={'2':'dos','3':'tres'}
j=dicValue.values()
print(j)
#clear
#copy() copia el diccionario
#fromkeys()     asigna el mismo valor a las keys
print(dict.fromkeys(['a','b','c'],2))
#get() recibe como parametro una key y si lo encuentra devuleve el valor y si no lo encuentra devuelve un none
varDic={'2':"El dos", '3':'El tres'}
print(varDic.get('3'))
#pop recibe un pametro y elimina la key
varDic={'2':"El dos", '3':'El tres','4':'El cuatro'}
varDic.pop('3')
print(varDic)
#setdefault()   tiene dos funcionalidades
varDic={'2':"El dos", '3':'El tres','4':'El cuatro'}
varDic.setdefault('phone','otra cosa')
print(varDic)
#udate(9 si encuentra la clave la acutailiza sinó la agrega    y recibe como parámetro un diccionario
varDic={'2':"El dos", '3':'El tres','4':'El cuatro'}
varDic.update({'2':'come pan'})
print(varDic)















