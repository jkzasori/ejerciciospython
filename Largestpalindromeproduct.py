"""
A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.
"""
def palabraAlrev(palabra):
    return palabra[::-1]
def palindromo(palabra):
    res=""
    longitudPalabra=len(palabra)
    palabra2=palabraAlrev(palabra)
    if longitudPalabra%2==0:
        mitad = int((longitudPalabra) / 2)
    else:
        mitad = int((longitudPalabra-1)/2)
        if(mitad==0):
            mitad=1
        #print("La mitad es ", mitad)
    lista = []
    lista2 = []
    for i in palabra:
        lista.append(i)
    for i in palabra2:
        lista2.append(i)
    cont=0
    for i in range(0, mitad):
        if lista[i]==lista2[i]:
            cont=cont+1
    if (cont/mitad)*100 == 100:
        #print("Es palindromo")
        res="si"
    if (cont/mitad)*100 <80:
        #print("No es palíndromo")
        res="no"
    return res
mayor=0
for i in range(999, 99, -1):
    for j in range(999, 99, -1):
        multi=i*j
        if palindromo(str(multi))=="si":
            break;
    if multi>=mayor:
        mayor=multi
print("*******************Este es el valor: ", mayor,"*******************")